$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})


$(function() {
  $('#class_type').change(function(){
    $('.colors').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#Payment_type').change(function(){
    $('.colors2').hide();
    $('#' + $(this).val()).show();
  });

  $('#billing_type').change(function(){
    $('.colors3').hide();
    $('#' + $(this).val()).show();
  });

  $('#payout_method_type').change(function(){
    $('.colors4').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#payout_method_type').change(function(){
    if($(this).val() == 'revenue-share-option'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
    
    }
    else if($(this).val() == 'hourly-option')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
    }
  });

  $('#Payment_type').change(function(){
    if($(this).val() == 'invoiced_account'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
        $( "#halt_date" ).hide();
    }
    else if($(this).val() == 'parent_paid')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
        $( "#halt_date" ).show();
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
      $( "#halt_date" ).show(); 
    }
  });

  
  $(document).ready(function(){
    $(".linkround-blk").click(function(){
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });

    $(".cancel-both-popup").click(function(){
      $('#add-calendar-event').modal('hide');
    });

    $(".linkround-blk").click(function(){
     
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });
    

  });

});
  
$(document).ready(function () {
  autoPlayYouTubeModal();
});

$('.multiple-select-date').datepicker({
  multidate: true,
  format: 'dd-mm-yyyy'
});

var array = ["2018-02-14","2018-01-15","2013-01-26"]
$('.redcolor-select-option').datepicker({
  multidate: true,
  datesDisabled: ['01/02/2019', '02/21/2019','01/14/2017']
  
});

/***  redcolor */

/*** end of redcolor */

function autoPlayYouTubeModal() {
      var trigger = $('.videoModalTriger');
  trigger.click(function () {
      var theModal = $(this).data("target");
      var videoSRC = $(this).attr("data-videoModal");
      var videoSRCauto = videoSRC + "?autoplay=1";
      $(theModal + ' iframe').attr('src', videoSRCauto);
      $(theModal).on('hidden.bs.modal', function (e) {
          $(theModal + ' iframe').attr('src', '');
      });
  });
};

$(document).ready(function(){  
  $(".sidebar-mobile-main-toggle").click(function(){
    //$('#navbar-mobile').hide();
    $('.nav-mobile').addClass('collapsed');
    $('.navbar-collapse.collapse').removeClass('in');
    
  });
  $(".open-unavailable-modal").click(function(){
    $("#updateevent-calendar-event").modal('show');
    $("#more-option-modal").modal('hide');
  });  
});

$(document).ready(function () {
  $('#passcheckbox1').change(function () {
    $('.pass-none').fadeToggle();
  });
});
