$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})


$(function() {
  $('#class_type').change(function(){
    $('.colors').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#Payment_type').change(function(){
    $('.colors2').hide();
    $('#' + $(this).val()).show();
  });

  $('#billing_type').change(function(){
    $('.colors3').hide();
    $('#' + $(this).val()).show();
  });

  $('#payout_method_type').change(function(){
    $('.colors4').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#timesheet_method_type').change(function(){
    // $('.hourly-view-filled').hide();
    if ($(this).val() == 'revenue-share-opt1') { 
      
      $('#hours, #pay_rate, #payout').addClass('hidden-value');
      $('#hours, #pay_rate, #payout').removeClass('show-value');
      $('.rev-view-filled').addClass('show-value');

      $('.inner-hourly').addClass('hidden-value');
      $('.inner-hourly').removeClass('show-value');
      $('.inner-default').addClass('show-value');
    } 
    else {
      /*$('#hours, #pay_rate, #payout').addClass('show-value');
      $('#rev_share, #hourly').addClass('hidden-value');*/

      $('#hours, #pay_rate, #payout').addClass('show-value');
      $('.rev-view-filled').removeClass('show-value');

      $('.inner-hourly').addClass('show-value');
      $('.inner-default').addClass('hidden-value');
      $('.inner-default').removeClass('show-value');
    }
  }); 

  
  /*
  $('#timesheet_method_type').change(function(){
    if ($(this).val() == 'hourly-opt2') {
      $('#hours, #pay_rate, #payout').removeClass('hourly-view-filled');
      $('#rev_share, #hourly').addClass('hourly-view-filled');
      $('.inner-hourly').addClass('show-value');
      $('.inner-default').addClass('hidden-value');
      $('.inner-default').removeClass('show-value');

    } 
    else {
      $('#hours, #pay_rate, #payout').addClass('hourly-view-filled');
      $('#rev_share, #hourly').removeClass('hourly-view-filled');
     
      $('.inner-hourly').addClass('hidden-value');
      $('.inner-hourly').removeClass('show-value');
      $('.inner-default').addClass('show-value');
    }
  }); */

  $('#payout_method_type').change(function(){
    if($(this).val() == 'revenue-share-option'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
    
    }
    else if($(this).val() == 'hourly-option')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
    }
  });

  $('#Payment_type').change(function(){
    if($(this).val() == 'invoiced_account'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
        $( "#halt_date" ).hide();
    }
    else if($(this).val() == 'parent_paid')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
        $( "#halt_date" ).show();
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
      $( "#halt_date" ).show(); 
    }
  });

  
  $(document).ready(function(){
    $(".linkround-blk").click(function(){
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });

    $(".cancel-both-popup").click(function(){
      $('#add-calendar-event').modal('hide');
    });

    $(".linkround-blk").click(function(){
     
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });
    

  });

});
  
$(document).ready(function () {
  autoPlayYouTubeModal();
});

$('.multiple-select-date').datepicker({
  multidate: true,
  format: 'dd-mm-yyyy'
});

var array = ["2018-02-14","2018-01-15","2013-01-26"]
$('.redcolor-select-option').datepicker({
  multidate: true,
  datesDisabled: ['01/02/2019', '02/21/2019','01/14/2017']
  
});

/***  redcolor */

/*** end of redcolor */

function autoPlayYouTubeModal() {
      var trigger = $('.videoModalTriger');
  trigger.click(function () {
      var theModal = $(this).data("target");
      var videoSRC = $(this).attr("data-videoModal");
      var videoSRCauto = videoSRC + "?autoplay=1";
      $(theModal + ' iframe').attr('src', videoSRCauto);
      $(theModal).on('hidden.bs.modal', function (e) {
          $(theModal + ' iframe').attr('src', '');
      });
  });
};

$(document).ready(function(){  
  $(".sidebar-mobile-main-toggle").click(function(){
    //$('#navbar-mobile').hide();
    $('.nav-mobile').addClass('collapsed');
    $('.navbar-collapse.collapse').removeClass('in');
    
  });
  $(".open-unavailable-modal").click(function(){
    $("#updateevent-calendar-event").modal('show');
    $("#more-option-modal").modal('hide');
  });  
});

// .open-unavailable-modal




$(document).ready(function () {
  $('#passcheckbox1').change(function () {
    $('.pass-none').fadeToggle();
  });
});

/****   checkbox select in DELETE A SESSION Popup   ****/

$(".checkbox_select_box").click(function() {
  
  if($(this).is(":checked")) {
    $(".delete-refund-btnshow").show();
    $(".delete-sbutton").hide();
  } 
  else {
    $(".delete-sbutton").show();
    $(".delete-refund-btnshow").hide();
  }

});

/********/

// $('#colorpalettediv').colorPalettePicker({
//   bootstrap: 3,
//   lines: 4
// });


$("#yes_no_toggle").on("click", function(){
  $("#reserve_accounts_modal").modal('show');
  $("#view-leads-details").modal('hide');
});
$(".close_cancel_viewlead").click(function(){
  $(".btn-toggle-yesno").removeClass("active");
});




$(document).ready(function() {

  $(".hover_rotation_modal").hover( function(){
    $("#sport_rotation").modal('show').fadeIn(300);
    $("#add-data-modal1").modal('hide');
  });

  /* lionmail */

  $('.user-searchbox').keyup(function() {
    if ($(this).val().length == 0) {
      $('.display-none').hide();
      $(".search-input-group").removeClass("radius-search");
    } else {
      $('.searchbox-result-listing').show();
      $(".search-input-group").addClass("radius-search");
    }
  }).keyup();

  $('.checkbox-inner1').on('click', function(event){
    
    if($(this).prop("checked") == true){
      $(this).parents().eq(1).addClass('active');
    }
    else{
      $(this).parents().eq(1).removeClass('active');
    }

  }); 

  $('.starred-div .btnstar-click').on('click', function(){
      $(this).html('<i class="material-icons icon-star"> star_border </i>').parent().siblings();
      $(this).toggleClass("addstar");
      $(".addstar").html('<i class="material-icons icon-star icon-active"> star </i>');
  }); 
  
  $("#compose-modal").on("click", function(){
    $(".compose-modal").addClass("display_block");
    $(".cd-shadow-overlayer").show();
  });
  $(".btn-close").click(function(){
    $(".compose-modal").removeClass("display_block");
    $(".cd-shadow-overlayer").hide();
  });
  $(".cd-shadow-overlayer").click();

});



// file uploader
