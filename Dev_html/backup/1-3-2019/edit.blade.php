@extends('parent.layouts.frontend')
@section('title','Edit Profile')
@section('content')

<div class="middle-container clearfix">

    <section class="edit-profile-section clearfix">
        <div class="edit-profile-div clearfix">
            <div class="container-fluid">
                <div class="row ">
                    
                    <div class="col-md-3 col-sm-4">
                        <div class="left-sidebar clearfix">
                            <div class="card-div clearfix">
                                <div class="img-thumb-div">
                                    <img @if(@getimagesize(Auth::user()->image)) src="{{ Auth::user()->image }}?v={{ time() }}"  @else src="{{ asset('assets/frontend-parent/images/default.png') }}" @endif alt="user-profile" class="img-responsive img-profile-center">
                                    <a href="javascript:void(0);" class="open_profile_modal"  ><span class="round-edit"><img src="{{ asset('assets/frontend-parent/images/icons/edit-blue.png') }}" class="img-responsive edit-image"></span></a>
                                </div>
                                
                                <div class="div-profile">
                                    <h3>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h3>
                                    <address>
                                        <p>{{ Auth::user()->profile->address }},</p>
                                        <p>{{ Auth::user()->profile->city }} , {{ Auth::user()->profile->state }} - {{ Auth::user()->profile->zipcode }} </p>
                                        <p><a href="tel:{{ Auth::user()->profile->cellphone }}">{{ Auth::user()->profile->cellphone }}</a></p>
                                        <p><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></p>
                                    </address>    
                                   
                                </div>    
                            </div><!-- end of card-div -->

                        </div><!-- end of left-sidebar -->
                    </div><!-- End of col-md-3 --> 

                    <div class="col-md-9 col-sm-8">
                      
                        <div class="right-sidebar-edit clearfix">
                            
                            <section class="tab-section clearfix">
                                <div class="tab-div clearfix">
                                    <ul class="nav nav-pills nav-justified clearfix">
                                        <li class="active"><a data-toggle="pill" href="#about">About</a></li>
                                        <li><a data-toggle="pill" href="#payment">Payment Methods</a></li>
                                    </ul>
                                    <div class="tab-content tab-content-div clearfix">
                                        <div id="about" class="tab-pane fade in active">
                                            <div id="r_tets" class="info-card-01 clearfix">
                                                <div class="tabhead-div clearfix">
                                                    <h3> Basic Information </h3>
                                                    <p><a href="javascript:void(0);" id="e_basic" >Edit</a></p>
                                                </div><!-- end of tabhead-div -->
                                                {{ Form::open(['route' => ['profile.update']]) }}
                                                <div class="table-div clearfix">
                                                    
                                                    <ul class="clearfix">
                                                        <li class="first-column"> First Name </li>    
                                                        <li class="second-column space_basic" > 
                                                            <span>{{ Auth::user()->first_name }}</span>
                                                            <input type="text" class="show_hide form-control" name="first_name" required value="{{ Auth::user()->first_name }} " />
                                                        </li>
                                                    </ul>
                                                    <ul class="clearfix">
                                                        <li class="first-column"> Last Name </li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->last_name }} </span>
                                                            <input type="text" class="show_hide form-control" name="last_name" required value="{{ Auth::user()->last_name }}" />
                                                        </li>
                                                    </ul>    

                                                </div>
                                                <input type="hidden" name="basic_info" value="true">
                                                <div class="display_class2">
                                                <input type="submit" class="btn btn-primary btn-custom btn-update up_class" value="Update">
                                                </div>
                                                {{ Form::close() }}

                                            </div>  <!-- end of info-card-01 -->

                                            <div id="r_tets2" class="info-card-01 info-card-02 clearfix">
                                                <div class="tabhead-div clearfix">
                                                    <h3> Contact Information </h3>
                                                    <p><a href="javascript:void(0);" id="r_con">Edit</a></p>
                                                </div><!-- end of tabhead-div -->
                                                {{ Form::open(['route' => 'profile.update', 'id' => 'edit-form' ]) }}
                                                <div class="table-div clearfix">
                                                    <ul class="clearfix">
                                                        <li class="first-column"> Phone</li>    
                                                        <li class="second-column space_basic" > 
                                                            <span>{{ Auth::user()->profile->cellphone }}</span>
                                                            <input type="text" class="show_hide form-control" name="cellphone" value="{{ Auth::user()->profile->cellphone }}" id="phone" pattern="\d{10}">
                                                        </li>
                                                    </ul>                                                  
                                                    <ul class="clearfix">
                                                        <li class="first-column">Address</li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->profile->address }}</span>
                                                            <input type="text" class="show_hide  form-control" name="address" value="{{ Auth::user()->profile->address }}" />
                                                        </li>
                                                    </ul>
                                                    <ul class="clearfix">
                                                        <li class="first-column">City</li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->profile->city }}</span>
                                                            <input type="text" name="city" class="show_hide  form-control" value="{{ Auth::user()->profile->city }}" />
                                                        </li>
                                                    </ul>
                                                    <ul class="clearfix">
                                                        <li class="first-column">State</li>    
                                                        <li class="second-column space_basic" >
                                                            <span>{{ Auth::user()->profile->state }}</span>
                                                            @include('parent.component.state-picker', ['class' => 'show_hide form-control', 'name' => 'state', 'selected' => Auth::user()->profile->state])
                                                        </li>
                                                    </ul>
                                                    <ul class="clearfix">
                                                        <li class="first-column">Zipcode</li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->profile->zipcode }}</span>
                                                            <input type="text" class="show_hide form-control" name="zipcode" value="{{ Auth::user()->profile->zipcode }}"  /></li>
                                                    </ul>
                                                    <ul class="clearfix">
                                                        <li class="first-column">Emergency Contact Number</li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->emergency->phone }}</span>
                                                            <input type="text" class="show_hide form-control" name="emergency[phone]" value="{{ Auth::user()->emergency->phone }}" id="e_phone" />
                                                        </li>
                                                    </ul>
                                                    <ul class="clearfix">
                                                        <li class="first-column">Emergency Contact Person First Name</li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->emergency->first_name }}</span>
                                                            <input type="text" class="show_hide form-control" name="emergency[first_name]" value="{{ Auth::user()->emergency->first_name }}" />
                                                        </li>
                                                    </ul>    
                                                    <ul class="clearfix">
                                                        <li class="first-column">Emergency Contact Person Last Name</li>    
                                                        <li class="second-column space_basic">
                                                            <span>{{ Auth::user()->emergency->last_name }}</span>
                                                            <input type="text" class="show_hide form-control" 
                                                            name="emergency[last_name]" value="{{ Auth::user()->emergency->last_name }}" />
                                                        </li>
                                                    </ul>
                                                    <input type="hidden" name="contact_info" value="true">
                                                    <div class="display_class2">
                                                    <input type="submit" class="btn btn-primary btn-custom btn-update up_class" value="Update">
                                                    </div>
                                                </div>
                                                {{ Form::close() }}
                                            </div>  <!-- end of info-card-01 -->
                                        </div> <!-- end of about -->   

                                        <div id="payment" class="tab-pane fade">
                                            <h3 class="profile-edit-title">Payment Methods</h3>
											
											<div class="payment-method-tbl">
											<div class="table-responsive clearfix">
											
												<table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<th width="30%">Method</th>
													<th width="20%">Number</th>
													<th width="20%">Expires</th>
													<th width="30%">&nbsp;</th>
                                                </tr>
                                                @foreach(Auth::user()->cards as $card)
												<tr>
													<td><img src="{{ asset('assets/frontend-parent/images/icons/visa-pay-logo.svg') }}" alt="" class="visa-icon">{{ ucfirst($card->brand) }} </td>
													<td>ending in {{ $card->number }}</td>
													<td>{{ $card->exp_month }}/{{ $card->exp_year }}</td>
													<td> 
                                                        @if($card->default)
                                                        <span class="default-span"><button class="default">Default</button></span>
                                                        @else
                                                            {{ Form::open(['route' => ['card.setdefault', $card->card_id], 'method' => 'put']) }}
                                                            <span class="set-default-span"><button type="submit" class="set-default"> Set Default </button></span>
                                                            {{ Form::close() }}
                                                            {{ Form::open(['route' => ['card.remove',$card->card_id], 'method' => 'delete']) }}
                                                            <span class="remove"><button class="remove-card"><img src="{{ asset('assets/frontend-parent/images/icons/delete.png') }}" alt=""></button></span>
                                                            {{ Form::close() }}
                                                        @endif
                                                    </td>
												</tr>
												@endforeach
												</table>
											
											</div>
											</div>
											@include('parent.component.add-card-stripe')
                                        </div><!-- end of payment  -->

                                    </div>  <!-- end of tab-content-div  -->
                                </div>  <!-- end of tab-div -->
                            </section>  <!-- end of tab-section -->

                        </div><!-- end of right-sidebar -->    

                    </div><!-- End of col-md-9 --> 

                </div>
            </div><!-- End of container-fluid -->     
        </div><!-- End of profile-div -->
    </section><!-- End of profile-section -->    
    

</div><!-- end of middle-container -->

@include('parent.modal.uploadimage')

@include('parent.include.flash-error')

@if(Session::has('profile_updated'))
    @include('parent.include.flash-success', ['message' => 'Your Profile has been updated successfully'])
@endif

@if(Session::has('image_updated') && (Session::has('image_updated') === true))
    @include('parent.include.flash-success', ['message' => 'Profile Image Changed Successfully'])
@endif

@if(Session::has('card_added') && (Session::has('card_added') === true))
    @include('parent.include.flash-success', ['message' => 'Stripe Payment Card Added Successfully.'])
@endif

@if(Session::has('card_deleted') && (Session::has('card_deleted') === true))
    @include('parent.include.flash-success', ['message' => 'Your Stripe Payment Card Deleted Successfully.'])
@endif

@endsection

@include('parent.profile.js.edit-js')

@include('parent.profile.js.payment-method-js')

