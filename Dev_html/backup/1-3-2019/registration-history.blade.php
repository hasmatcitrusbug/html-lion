@extends('parent.layouts.frontend')
@section('title', 'Registration History')

@section('content')
<div class="middle-container findclass-detail clearfix">

    <section class="header-section">
        <div class="header-div findclass-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h1>Purchased Classes</h1>
                        <div class="border-line"></div>
                    </div>
                </div>
            </div><!-- end of container-fluid -->
        </div><!-- end of header-div -->
    </section> <!-- end of header-section -->   

    <section class="purchased-section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="purchased-div clearfix">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#home">Current Classes</a></li>
                            <li><a data-toggle="pill" href="#menu1">Previous Registrations</a></li>
                        </ul>
                          
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                
                                <div class="tab-main-area">
                                    <div class="table-responsive">       
                                        
                                        <table class="table table-striped custom-table-class">
                                            <thead>
                                              <tr>
                                                <th>Class Title	</th>
                                                <th>Child Name	</th>
                                                <th>Coach</th>
                                                <th>Price </th>
                                                <th>Date Time </th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($activeClasses) == 0)
                                                <tr>
                                                    <td class="td01" colspan="5" ><p class="no-class-found">No Active Classes Found</p></td>
                                                </tr>
                                                @endif
                                                @foreach($activeClasses as $class)
                                                <tr>
                                                    <td class="td01">{{ $class->event->title }}</td>
                                                    <td class="td02">{{ $class->children->name }}</td>
                                                    <td class="td03">{{ $class->event->user->name }}</td>
                                                    <td class="td04"> $ {{ $class->payment->amtpaid }}</td>
                                                    <td class="td05">{{ $class->created_at->format(config('lfk.dtformat')) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                          </table>
                                    </div> <!-- table responsive -->
                                </div>

                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="tab-main-area">
                                    <div class="table-responsive">       
                                        
                                        <table class="table table-striped custom-table-class">
                                            <thead>
                                              <tr>
                                                <th>Class Title	</th>
                                                <th>Child Name	</th>
                                                <th>Coach</th>
                                                <th>Price </th>
                                                <th>Date Time </th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                 @if(count($expiredClasses) == 0)
                                                <tr>
                                                    <td class="td01" colspan="5"><p class="no-class-found">You have no Past Registrations</p></td>
                                                </tr>
                                                @endif
                                                @foreach($expiredClasses as $class)
                                                <tr>
                                                    <td class="td01">{{ $class->event->title }}</td>
                                                    <td class="td02">{{ $class->children->name }}</td>
                                                    <td class="td03">{{ $class->event->user->name }}</td>
                                                    <td class="td04"> $ {{ $class->payment->amtpaid }}</td>
                                                    <td class="td05">{{ $class->created_at->format(config('lfk.dtformat')) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                          </table>

                                    </div> <!-- table responsive -->
                                </div>


                            </div>
                        </div>

                    </div><!-- End of purchased-div -->
                </div>
            </div><!-- end of row -->
        </div><!-- End of container-fluid -->     
    </section><!-- End of purchased-section -->    
    
</div><!-- end of middle-container -->
@endsection