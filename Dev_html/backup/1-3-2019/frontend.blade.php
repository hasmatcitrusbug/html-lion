<!DOCTYPE html>

<html class='no-js' lang='en'>

<head>
  <meta name="description" content="" />
  <meta name="author" content="" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <title>@yield('title') - Lionheart Fitness Kids</title>

  <meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
  <meta content='yes' name='apple-mobile-web-app-capable'>
  <meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>
  <link href='{{ asset('assets/frontend-parent/images/favicon.ico') }}' rel='icon' type='image/ico'>

  @include('parent.include.css')

</head>

<body>
  
  @auth
    @include('parent.include.right-menu')
  @endauth

  @guest
    @include('parent.include.logout-menu')
  @endguest

  <div id="wrapper">

  @include('parent.include.header')

      @yield('content') @yield('footer')
   
  </div>
  <!-- end of wrapper -->
  
  @include('parent.include.js')

</body>

</html>