@extends('parent.layouts.frontend')
@section('title','View Profile')
@section('content')

<div class="middle-container profile clearfix">

    <section class="header-section">
        <div class="header-div">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="thumb-images">
                            <a href="#" data-toggle="modal" data-target="#image-modal"><img  @if(@getimagesize(Auth::user()->image)) src="{{ Auth::user()->image }}?v={{ time() }}" @else src="{{ asset('assets/frontend-parent/images/default.png') }}" @endif alt="user-profile" class="img-responsive img-profile"></a>
                        </div>    
                    </div>
                </div>
            </div><!-- end of container-fluid -->
        </div><!-- end of header-div -->
    </section> <!-- end of header-section -->   

    <section class="profile-section clearfix">
        <div class="profile-div clearfix">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-md-3 col-sm-4">
                        <div class="left-sidebar clearfix">
                            <div class="card-div clearfix">
                                <h3>{{ Auth::user()->name }} </h3>
                                <address>
                                    <p>{{ Auth::user()->profile->address }} ,</p>
                                    <p>{{ Auth::user()->profile->city }} , {{ Auth::user()->profile->state }} - {{ Auth::user()->profile->zipcode }}</p>
                                    <p><a href="tel:{{ Auth::user()->profile->cellphone }}">{{ Auth::user()->profile->cellphone }}</a></p>
                                    <p><a href="mailto:ppfl@yopmail.com">{{ Auth::user()->email }}</a></p>
                                </address>    
                                
                                <a href="#" data-toggle="modal" data-target="#enrollment-modal" class="btn btn-primary btn-custom">Auto Enrollment</a>
                                    
                                <span data-placement="right"  data-tooltip="Parents, please note you can opt-in and out of auto-enrollment at any time. To opt-in simply click the auto-enrollment box in the shopping cart when you are registering for a class. Your child will be automatically added to the roster for each new session at your specified class location and your credit card will be billed 7 days prior to the scheduled start date. To cancel simply click the Auto-Enrollment button on your profile and cancel. IMPORTANT: If you wish to cancel, make sure you do so at least 8 days before the class start date, once your card has been billed you will not be eligible for a refund."><i class="fa fa-question-circle i-qus"></i></span>

                            </div><!-- end of card-div -->
                            
                                <div class="card-div card-div-2 clearfix">
                                    
                                    <div class="top-head-1 clearfix">
                                        <h3> My  Coaches </h3>
                                    </div><!-- end of top heading -->

                                    @foreach($coaches as $coach)
                                    <div class="listing-profile-1 clearfix">
                                        <a href="{{ route('coach.view', ['username' => $coach->username]) }}">
                                            <div class="img-left">
                                                <img @if(@getimagesize($coach->image)) src="{{ $coach->image }}?v={{ time() }}"  @else src="{{ asset('assets/frontend-parent/images/default.png') }}"  @endif alt="{{ $coach->name }}" class="img-responsive user-img-1" >
                                            </div>
                                        </a>        
                                        <div class="right-content">
                                            <p>{{ $coach->name }} </p>
                                            <a href="#"  data-id="{{ $coach->id }}" data-homephone="{{ $coach->personalApplication->homephone or ''}}" data-toggle="modal" data-target="#contact-coach-modal" class="btn btn-primary btn-xs contact-coach">Contact Coach</a>    
                                        </div>
                                    </div><!-- end of list-1 -->
                                    @endforeach
                                    
                                </div><!-- end of card-div-2 -->
                            

                            <div class="schedule-div">
                                <div class="card-div card-div-3 clearfix">
                                    <div id='calendar' class="cal-div"></div>
                                    <div class="cal-event-blk">
                                        <span class="event-1"><span class="blue"></span> Class Days</span>
                                        <span class="event-1"><span class="red"></span> No Class Days</span>
                                    </div>
                                </div>    
                            </div>    

                        </div><!-- end of left-sidebar -->
                    </div><!-- End of col-md-3 --> 

                    

                    <div class="col-md-9 col-sm-8">
                        
                        <div class="right-sidebar clearfix">
                            
                            <div class="cardrow-01 clearfix">
                                <div class="card-div-01 half-card half-card1 clearfix">

                                    <div class="top-head-1 clearfix">
                                        <h3>Upcoming Classes</h3>
                                    </div>
                                    
                                    <div class="main-listing clearfix">
                                    @if(Auth::user()->UpcomingClass->count() == 0 )
                                        <div class="listing-profile-1 border-bottom-1 clearfix">
                                            <div class="w-60">
                                                <p>Your Coach has not posted the next session yet.</p>
                                            </div>
                                        </div><!-- end of list-1 -->
                                    @endif

                                    @foreach(Auth::user()->UpcomingClass->pluck('event')->unique()->values() as $event)
                                        @if($event)
                                        <div class="listing-profile-1 border-bottom-1 clearfix">
                                            <div class="w-60">
                                                <p>{{ $event->title }}</p>
                                            </div>
                                            <div class="w-25">
                                                <p>{{ $event->class_start->format(config('lfk.date_format')) }}</p>
                                            </div>
                                            <div class="w-15">
                                                <p><a href="{{ route('class.view', $event->alias) }}">
                                                    <span >View</span></a> </p>
                                            </div>
                                        </div><!-- end of list-1 -->
                                        @endif
                                    @endforeach
                                    
                                    </div><!-- end of main-listing -->
                                </div><!-- end of card-div-01 -->

                                <div class="card-div-01 half-card half-card2 clearfix">

                                    <div class="top-head-1 clearfix">
                                        <h3>Discounts</h3>
                                    </div>
                                    
                                    <div class="main-listing clearfix">
                                        <div class="listing-profile-1 border-bottom-1 clearfix">
                                            <div class="w-100">
                                                @if( $coupon_count_per == 0)
                                                <p>You currently do not have any credits available in your account.</p>
                                                @else
                                                <p>You currently have a  $ {{ $coupon_count_per }}  credit available in your account. Discount will be automatically applied when you register for your next class.</p>
                                                @endif
                                            </div>
                                        </div><!-- end of list-1 -->
                                    </div><!-- end of main-listing -->
                                </div><!-- end of card-div-01 -->
                            
                            </div><!-- end of row -->

                            <div class="card-div-01 card-div-02 clearfix">
                                
                                <div class="top-head-1 top-head-02 clearfix">
                                    <h3>My Kids</h3>
                                    <!--<p><a href="#">+ ADD</a></p>-->
									<p><a href="#" data-toggle="modal" data-target="#add-one">+ ADD</a></p>
                                </div><!-- end of top heading -->
                                
                                <div class="main-listing clearfix">
                                    @if(Auth::user()->children->count() == 0 )
                                        <div class="listing-profile-1 border-bottom-1 clearfix">
                                            <div class="w-60">
                                                <p>No Kids are added Yet</p>
                                            </div>
                                        </div><!-- end of list-1 -->
                                    @endif
                                    @foreach(Auth::user()->children as $child)
                                    
                                    <div class="listing-profile-1 border-bottom-1 clearfix">
                                        <div class="w-60">
                                            <p>{{ $child->name }}</p>
                                        </div>
                                        <div class="w-25">
                                            <p>{{ $child->years }} Years</p>
                                        </div>
                                       
                                        <div class="w-15">
                                            <p>
                                            <a href="#" data-toggle="modal" data-target="#editChildModal-{{ $child->id }}">
                                                <img src="{{ asset('assets/frontend-parent/images/icons/edit-blue.png') }}" class="img-responsive i-add">
                                            </a>
                                            {{ Form::open(['route' => ['child.remove', $child->id], 'method' => 'delete']) }}
                                                <button type="submit" class="del-child">
                                                    <img src="{{ asset('assets/frontend-parent/images/icons/garbage-blue.png') }}" class="img-responsive i-delete ml-30">
                                                </button>
                                            {{ Form::close() }} 
                                           
                                            </p>
                                        </div>
                                    </div><!-- end of list-01 -->
                                    @include('parent.modal.edit-child',compact('child'))
                                    @endforeach
                                   
                                </div><!-- end of main-listing -->
                            </div><!-- end of card-div-02 -->

                            <div class="card-div-01 card-div-02 card-div-03 clearfix">
                                
                                <div class="top-head-1 top-head-02 clearfix">
                                    <h3>Recent Registrations</h3>
                                    <p><a href="{{ route('myaccount.registrationhistory') }}">All Activity</a></p>
                                </div><!-- end of top heading -->
                                
                                <div class="main-listing clearfix">
                                    @if(Auth::user()->rosters->count() == 0)
                                    <div class="listing-profile-1 border-bottom-1 clearfix">
                                            <div class="w-60">
                                                <p>No Recent Registrations</p>
                                            </div>
                                        </div><!-- end of list-1 -->    
                                    @endif
                                    @foreach(Auth::user()->rosters->take(10)->sortByDesc('created_at') as $roster)
                                        @if($roster->children && $roster->event)
                                            <div class="listing-profile-1 border-bottom-1 clearfix">
                                                <div class="w-88">
                                                    <p>You have a recent payment for child {{ $roster->children->name }} for class {{ $roster->event->title }}</p>
                                                </div>
                                                <div class="w-12">
                                                    <p>{{ $roster->created_at->diffForHumans(Carbon::now()) }}</p>
                                                </div>
                                            </div><!-- end of list-01 -->
                                        @endif
                                    @endforeach
                                   
                                </div><!-- end of main-listing -->
                            </div><!-- end of card-div-03 -->


                        </div><!-- end of right-sidebar -->    

                    </div><!-- End of col-md-9 --> 

                </div>
            </div><!-- End of container-fluid -->     
        </div><!-- End of profile-div -->
    </section><!-- End of profile-section -->    
    
</div><!-- end of middle-container -->

@if(@getimagesize(Auth::user()->image))
    @php $photo =  Auth::user()->image ; @endphp  
@else 
    @php $photo = asset('assets/frontend-parent/images/default.png') ; @endphp   
@endif

@include('parent.modal.image-modal', ['image' => $photo] )

@include('parent.modal.add-child')

@include('parent.modal.auto-enrollment',compact('enrollments'))

@include('parent.modal.contact-coach')

@include('parent.include.flash-error')

@if(Session::has('success'))
    @include('parent.include.flash-success', ['message' => __('messages.success.contactcoach')]) 
@endif

@if(Session::has('child_added'))
    @include('parent.include.flash-success', ['message' => 'Child has been added successfully'])
@endif

@if(Session::has('child_deleted'))
    @include('parent.include.flash-success', ['message' => 'Child has been deleted successfully'])
@endif

@if(Session::has('child_updated'))
    @include('parent.include.flash-success', ['message' => 'Child Updated successfully'])
@endif

@if(Session::has('delete_enrollment') && (Session::has('delete_enrollment') === true))
    @include('parent.include.flash-success', ['message' => 'Enrollment has been delete successfully'])
@endif

@endsection

@include('parent.profile.js.child-js')

@include('parent.profile.js.calender-js')
